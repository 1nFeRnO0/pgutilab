package com.example.task;

import java.util.Arrays;

public class Single_number_search{
    public static void main(String args[]){
        int[] a = {7,2,3,7,4,4,2,1,3};
        // sorting(a);
        // boolean flag = false;
        
        // for (int i = 0; i<a.length-1;i++){
        //     if(a[i] != a[i+1]){
        //         System.out.println(a[i]);
        //         flag = true;
        //         break;
        //     }
        //     else if(a[i] == a[i+1]){
        //         i++;
        //     }   
        // }
        // if(flag==false){
        //     System.out.println(a[a.length-1]);
        // }

        // РЕАЛИЗАЦИЯ ЧЕРЕЗ XOR
        int number = 0;
        for (int i=0;i<a.length;i++)
            number = number ^ a[i];
        System.out.println(number);
         
    }
    
    public static int[] sorting(int[] massive){
        Arrays.sort(massive);
        return massive;
    }
}