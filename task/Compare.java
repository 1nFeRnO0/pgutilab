package com.example.task;

import java.lang.Math;

public class Compare{
    public int comparePowers(int[] number1, int[] number2){
        return (int) Math.signum(number1[1] * Math.log((double)number2[0] / number1[0]) + (number2[1] - number1[1]) * Math.log(number2[0]));
    
    }
}