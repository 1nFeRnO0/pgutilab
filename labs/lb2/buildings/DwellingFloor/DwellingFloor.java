package com.example.lab.lb2.buildings.DwellingFloor;

import com.example.lab.lb2.buildings.Flat.*;

public class DwellingFloor {
    private Flat flats[]; 
    public DwellingFloor(Flat flats[]){
        this.flats = flats;
    }

    public int getFlatsCount(){
        return flats.length;
    }

    public double getFlatsSquare(){
        double sum = 0;
        for (Flat flat : flats) {
            sum += flat.getSquare();
        }
        return sum;
    }

    public int getFlatsRooms(){
        int sum = 0;
        for (Flat flat : flats) {
            sum += flat.getRooms();
        }
        return sum;
    }

    public Flat[] getFlats(){
        return flats;
    }

    public Flat getFlat(int num){
        if(num < flats.length && num >= 0)
            return flats[num];
        System.out.println("There is no such flat");
        System.exit(0);
        return null;
    }

    public void setFlat(Flat flat, int num){
        if(num < flats.length && num >= 0)
            flats[num] = flat;
        System.out.println("There is no such flat");
        System.exit(0);
    }

    public void addFlat(Flat flat, int num){
        if(num >= flats.length){
            Flat f[] = new Flat[num + 1];
            int i = 0;
            for (; i < flats.length; i++) {
                f[i] = this.flats[i];
            }
            for (; i < num + 1; i++) {
                f[i] = new Flat();
            }
            f[num] = flat;
            this.flats = f;
        }
        else{
            System.out.println("This flat already exist");
            System.exit(0);
        }
    }

    public void deleteFlat(int num){
        if(num < flats.length && num >= 0){
            Flat f[] = new Flat[this.flats.length - 1];
            int i = 0;
            for(; i < num; i++) f[i] = this.flats[i];
            for(; i < this.flats.length - 1; i++) f[i] = this.flats[i + 1];
        }
        System.out.println("There is no such flat");
        System.exit(0);
    }

    public Flat getBestSpace(){
        Flat f = new Flat();
        for (Flat flat : this.flats) {
            f = f.getSquare() > flat.getSquare() ? f : flat;
        }
        return f;
    }
}